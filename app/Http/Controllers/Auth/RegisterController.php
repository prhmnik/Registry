<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'FirstName' => 'Required|String|max:255',
            'LastName' => 'Required|String|max:255',
            'Username' => 'Required|String|max:255',
            'password' => 'Required|String|confirmed|min:6',
            'FatherId' => 'integer|nullable',
            'MotherId' => 'integer|nullable',
            'NationalCode' => 'Required|String|unique:users',
            'BirthDate' => 'date_format:Y-m-d',
            'DeathDate' => 'date_format:Y-m-d|nullable',
            'Gender' => 'Required|integer',
            'RoleId' => 'Required|integer',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        /* $fileName = null;
        // save images in directory
        if(Input::hasfile('profileimage')) {
            $image = Input::file('profileimage');
            $destinationpath = public_path() . '/uploads';
            $filename = time(). '.' . $image->getclientoriginalextension();

            $image->move($destinationpath, $filename);
        } */
        session()->flash('CreateUser', 'User created successfully.');
        return User::create([
                'FirstName' => $data['FirstName'],
                'LastName' => $data['LastName'],
                'username' => $data['Username'],
                'password' => Hash::make($data['password']),
                'FatherId' => $data['FatherId'],
                'MotherId' => $data['MotherId'],
                'NationalCode' => $data['NationalCode'],
                'BirthDate' => $data['BirthDate'],
                'DeathDate' => $data['DeathDate'],
                'Gender' => $data['Gender'],
                'RoleId' => $data['RoleId'],
                // 'ProfileImage' => $fileName,
        ]);
    }
}
