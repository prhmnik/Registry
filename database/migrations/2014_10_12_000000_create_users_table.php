<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('FirstName');
            $table->string('LastName');
            $table->string('Username');
            $table->string('Password');
            $table->integer('FatherId')->nullable();
            $table->integer('MotherId')->nullable();
            $table->string('NationalCode')->unique();
            $table->string('ProfileImage')->nullable();
            $table->string('BirthDate');
            $table->string('DeathDate')->nullable();
            $table->integer('Gender');
            $table->integer('RoleId')->unsigned();
            $table->rememberToken();
            $table->timestamps();
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
