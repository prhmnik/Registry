@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Report Marriages') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('ReturnMarriage') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="StartDate" class="col-md-4 col-form-label text-md-right">{{ __('StartDate') }}</label>

                            <div class="col-md-6">
                                <input id="StartDate" type="date" class="form-control{{ $errors->has('StartDate') ? ' is-invalid' : '' }}" name="StartDate" value="{{ old('StartDate') }}" required>

                                @if ($errors->has('StartDate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('StartDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="EndDate" class="col-md-4 col-form-label text-md-right">{{ __('EndDate') }}</label>

                            <div class="col-md-6">
                                <input id="EndDate" type="date" class="form-control{{ $errors->has('EndDate') ? ' is-invalid' : '' }}" name="EndDate" value="{{ old('EndDate') }}" required>

                                @if ($errors->has('EndDate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('EndDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Search') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @isset($Marriages)
                @if (count($Marriages) == 0)
                    <br>
                    <div class="alert alert-danger">
                        <strong>Marriages not found!</strong>
                    </div>
                @else
                <br>
                <ul class="list-group">
                    @foreach ($Marriages as $Marriage)
                        <li class="list-group-item list-group-item-action">
                            {{ $Marriage->HusbandFirstName}} {{ $Marriage->HusbandLastName}} //
                            {{ $Marriage->WifeFirstName}} {{ $Marriage->WifeLastName}} //
                            {{ $Marriage->MarriageDate}}
                        </li>
                    @endforeach
                </ul>
                @endif
            @endisset

        </div>
    </div>
</div>
@endsection
