@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Divorce') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('UpdateDivorce') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="HusbandId" class="col-md-4 col-form-label text-md-right">{{ __('Husband') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('HusbandId') ? ' is-invalid' : '' }}" id="HusbandId" name="HusbandId" value="{{ old('HusbandId') }}" required>
                                    <option value=""></option> 
                                    <option value="1">1</option> 
                                </select>

                                @if ($errors->has('HusbandId'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('HusbandId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="WifeId" class="col-md-4 col-form-label text-md-right">{{ __('Wife') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('WifeId') ? ' is-invalid' : '' }}" id="WifeId" name="WifeId" value="{{ old('WifeId') }}" required>
                                    <option value=""></option> 
                                    <option value="1">1</option> 
                                </select>

                                @if ($errors->has('WifeId'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('WifeId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="DivorceDate" class="col-md-4 col-form-label text-md-right">{{ __('DivorceDate') }}</label>

                            <div class="col-md-6">
                                <input id="DivorceDate" type="date" class="form-control{{ $errors->has('DivorceDate') ? ' is-invalid' : '' }}" name="DivorceDate" value="{{ old('DivorceDate') }}" required>

                                @if ($errors->has('DivorceDate'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('DivorceDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
